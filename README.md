# Clustering and Classifying People based on Text and KB information

## Introduction

In this project, we will collect information about a people belonging to different categories (such as artists, politician, sportspeople, etc.).
Based on this information, our will then try to automatically cluster
and classify these people into the correct categories.
We will use two sources of information, namely:

- The Wikipedia online encyclopedia

- The Wikidata knowledge base

## Get and store the data

The goal of the first step is to compile a text corpus from the Wikipedia online encyclopedia. This corpus will be made of plain text sentences, selected so as to have a roughly balanced corpus in terms of training data (each target category should be associated with the same number of sentences). We will focus on the 6 following categories: *architects, mathematicians, painters, politicians, singers and writers.* Concretely, We have to implement a Python program which takes as a an input:

- a number k of persons per category,

- a number n of sentences per person (persons whose wikipedia description is too short to have n sentences, should be ignored).

This extraction task can be realised through the following steps:

1. Create a list of persons our want to work with.

Those persons should fall into two main types: **artists (A)** and **non artists (Z)**.
Singers, writers and painters are of type A, while architects, politicians and mathematicians of type Z. For each category, select 30 persons of that category. So in total we should have a list of 180 persons (half of them are artists and half of them are not).
We can use the wikidata warehouse to find persons of the expected categories. More precisely, the Wikidata collection can be filtered out using the SPARQL language and the following end point: https://query.wikidata.org/ .
We can thus use the SPARQL wrapper python library to apply a SPARQL query to the Wikidata warehouse and retrieve the required item identifiers.

2. for each selected person, retrieve his/her Wikidata description and Wikipedia page title. This can be done using the wikidata API along with the `wptools` python library.

3. Once we have a list of wikipedia page titles, fetch (if it exists) the corresponding English wikipedia page, and extract the n first sentences of its content.

At the end of this process, we will have in memory, for each person we selected:

- the text of the corresponding Wikipedia page ;
- the corresponding data type (A or Z) and category ;
- the WikiData description.

Store these into a csv file and save it on our hard drive.

### Outline :

- Use the wikidata warehouse to find the persons we want to work with (`SPARQLwrapper` could help).

- Classify people according to their profession (architects, mathematicians, painters, politicians, singers and writers) and their type of profession (artist or non artist).

- Find for each of these people their WikiData description and their wikipedia page (need to use `wptools`).

- Store it into a CSV file.

## Pre-processing

Before applying machine learning algorithm to our data, we first need to process text. For each Wikipedia text collected, do the following:

- tokenize the text

- lowercase the tokens

- remove function words

Do the same for the Wikidata description and store the results in a
panda dataframe containing the following columns.

- column 1: person

- column 2: Wikipedia page text

- column 3: Wikipedia page text after preprocessing

- column 4: Wikidata description

- column 5: Wikidata description after preprocessing

Note. To improve clustering and classification results, feel free to
add further pre-processing steps (Named entity recognition, pos-
tagging and extraction of, nouns and verbs).

### Outline :

- clean up the data

- Store the data according to the above organisation 

## Clusturing

The goal of this step is to use the collected data (text and wikidata descriptions) to automatically cluster persons first, using 2 clusters and second, using 6 clusters and to compare the results obtained when using three ways of representing text (tokens, token frequency and tf-idf) and 2 vs. 6 clusters.
Our code have to include the following functions:

- a function to train a clustering algorithm on some data using N clusters and some input representation method M (tokens, token frequency and tf-idf). Data, M and N should be parameters of that function.

- a function to compute both intrinsic (Silhouette coefficient) and extrinsic (homogeneity, completeness, v-measure, adjusted Rand index) evaluation scores for clustering results.

- a function to visualise those metrics values for each of the three input representations (tokens, token frequency and tf-id) and for 2 vs. 6 clusters (so your visualisation should display 5 scores for each of the 6 clustering results).

### Outline :

- Use the collected data to automatically cluster in 2 clusters (Artist and non artist)

- Use the collected data to automatically cluster in 6 clusters (architects, mathematicians, painters, politicians, singers and writers)

- Using three ways of representing text (tokens, token frequency and tf-idf) and 2 vs. 6 clusters.

## Classifying

Since we know which category and subcategory each person in our dataset belongs to, we can also learn a classifier and check how well it can predict the category and subcategory a person in our dataset belongs to.
Our code should include:

- a function which outputs accuracy, a confusion matrix, precision,
  recall and F1 for the results of our classification (when classifying
  into categories and when classifying into subcategories)

- a function which outputs a visualisation of the accuracy of our
  classifier per category and per subcategories

### Outline :

- Create a function which outputs accuracy, a confusion matrix, precision,
  recall and F1

- Create a function which outputs a visualisation of the accuracy per category and per subcategories



------------------------

In order to make our program more understandable we have split it into several files (one for each exercise). To reproduce our results, you just have to run the files in order.

The notebook for exercise 1 will save two CSV files:

- **"export_dataframe.csv"** which contains all the data from the exercise and which we will use in the other exercises
- **"Exercise_1.csv"** which contains the information requested in the instructions for exercise 1

The notebook of exercise 2 will update **"export_dataframe.csv"** file to save the pre-processed data that we will  in the notebook for exercice 3.
